-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: task
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `c_auth_group`
--

DROP TABLE IF EXISTS `c_auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `post` enum('D','R','W','X') NOT NULL DEFAULT 'R' COMMENT 'Права доступа к почте',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_auth_group`
--

INSERT INTO `c_auth_group` VALUES (1,'Админ',NULL,'X'),(2,'Пользователь','Имеет учетку и доступ к почте на чтение','R'),(3,'Проверенный пользователь','Имеет доступ к почте на запись','W');

--
-- Table structure for table `c_auth_user`
--

DROP TABLE IF EXISTS `c_auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL DEFAULT b'0',
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `login` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `spam` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `c_auth_user_email_uindex` (`email`),
  UNIQUE KEY `c_auth_user_login_uindex` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_auth_user`
--

INSERT INTO `c_auth_user` VALUES (1,_binary '','adminov','admin','adminovich','admin@ad.min','+70001234567','admin','21232f297a57a5a743894a0e4a801fc3',_binary '\0'),(2,_binary '','userov','user','userovich','user@u.ser','+70001234568','user','ee11cbb19052e40b07aac0ca060c23ee',_binary ''),(3,_binary '','authorov','author','authorovich','author@auth.or','+70001234569','author','02bd92faa38aaa6cc0ea75e59937a1ef',_binary ''),(4,_binary '','testov','test','testovich','test@t.est','+70001234570','test','098f6bcd4621d373cade4e832627b4f6',_binary '');

--
-- Table structure for table `c_auth_user_group`
--

DROP TABLE IF EXISTS `c_auth_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_auth_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  KEY `c_auth_user_group_c_auth_group_id_fk` (`group_id`),
  KEY `c_auth_user_group_user_id_index` (`user_id`),
  CONSTRAINT `c_auth_user_group_c_auth_group_id_fk` FOREIGN KEY (`group_id`) REFERENCES `c_auth_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `c_auth_user_group_c_auth_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `c_auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_auth_user_group`
--

INSERT INTO `c_auth_user_group` VALUES (1,1),(2,2),(3,3),(4,3);

--
-- Table structure for table `c_post_message`
--

DROP TABLE IF EXISTS `c_post_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_post_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT '1',
  `author_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) NOT NULL DEFAULT 'Без темы',
  `text` text NOT NULL,
  `readed` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `c_post_message_c_auth_user_id_fk` (`author_id`),
  KEY `c_post_message_c_auth_user_id_fk_2` (`receiver_id`),
  KEY `c_post_message_c_post_section_id_fk` (`section_id`),
  CONSTRAINT `c_post_message_c_auth_user_id_fk` FOREIGN KEY (`author_id`) REFERENCES `c_auth_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `c_post_message_c_auth_user_id_fk_2` FOREIGN KEY (`receiver_id`) REFERENCES `c_auth_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `c_post_message_c_post_section_id_fk` FOREIGN KEY (`section_id`) REFERENCES `c_post_section` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_post_message`
--

INSERT INTO `c_post_message` VALUES (1,1,3,4,'2018-08-14 14:00:00','тема1','сообщение',_binary '\0'),(2,2,3,4,'2018-08-14 14:00:00','тема2','сообщение',_binary '\0'),(3,3,3,4,'2018-08-14 14:00:00','тема3','сообщение',_binary '\0'),(4,4,3,4,'2018-08-14 14:00:00','тема4','сообщение',_binary '\0'),(5,5,3,4,'2018-08-14 14:00:00','тема5','сообщение',_binary '\0'),(6,6,3,4,'2018-08-14 14:00:00','тема6','сообщение',_binary '\0'),(7,7,3,4,'2018-08-14 14:00:00','тема7','сообщение',_binary '\0'),(8,8,3,4,'2018-08-14 14:00:00','тема8','сообщение',_binary '\0'),(9,1,3,4,'2018-08-14 14:00:00','тема9','сообщение',_binary ''),(10,2,3,4,'2018-08-14 14:00:00','тема10','сообщение',_binary ''),(11,3,3,4,'2018-08-14 14:00:00','тема11','сообщение',_binary ''),(12,4,3,4,'2018-08-14 14:00:00','тема12','сообщение',_binary ''),(13,5,3,4,'2018-08-14 14:00:00','тема13','сообщение',_binary ''),(14,6,3,4,'2018-08-14 14:00:00','тема14','сообщение',_binary ''),(15,7,3,4,'2018-08-14 14:00:00','тема15','сообщение',_binary ''),(16,8,3,4,'2018-08-14 14:00:00','тема16','сообщение',_binary '');

--
-- Table structure for table `c_post_section`
--

DROP TABLE IF EXISTS `c_post_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_post_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `color` varchar(255) DEFAULT NULL COMMENT 'https://www.w3schools.com/colors/colors_picker.asp',
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `c_post_section_c_auth_user_id_fk` (`owner_id`),
  KEY `c_post_section_c_post_section_id_fk` (`parent_id`),
  CONSTRAINT `c_post_section_c_auth_user_id_fk` FOREIGN KEY (`owner_id`) REFERENCES `c_auth_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `c_post_section_c_post_section_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `c_post_section` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_post_section`
--

INSERT INTO `c_post_section` VALUES (1,NULL,'Основные','hsl(0, 100%, 80%)','2018-07-31 14:14:14',3),(2,1,'по работе','hsl(35, 100%, 80%)','2018-07-31 14:14:14',3),(3,1,'личные','hsl(70, 100%, 80%)','2018-07-31 14:14:14',3),(4,NULL,'Оповещения','hsl(105, 100%, 80%)','2018-07-31 14:14:14',3),(5,4,'форумы','hsl(140, 100%, 80%)','2018-07-31 14:14:14',3),(6,4,'магазины','hsl(175, 100%, 80%)','2018-07-31 14:14:14',3),(7,4,'подписки','hsl(210, 100%, 80%)','2018-07-31 14:14:14',3),(8,NULL,'Спам','hsl(245, 100%, 80%)','2018-07-31 14:14:14',3);
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-14 15:13:40
