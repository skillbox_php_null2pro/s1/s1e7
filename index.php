<?php require_once '.src/header.php' ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="left-collum-index">

            <h1>Возможности проекта —</h1>
            <p>Вести свои личные списки, например покупки в магазине, цели, задачи, и многое другое.
                Делиться списками с друзьями, и просматривать списки друзей.</p>


        </td>
        <td class="right-collum-index">
                <div class="project-folders-menu" <?=($CORE->Session->isAuth())?'hidden':''?>>
                    <ul class="project-folders-v">
                        <li class="project-folders-v-active"><span>Авторизация</span></li>
                        <li><a href="#">Регистрация</a></li>
                        <li><a href="#">Забыли пароль?</a></li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>
                <?php if (isset($_POST['login'])&&!$CORE->Session->isAuth()): ?>
                    <p class="fail">Учетная запись с этой комбинацией логина и пароля не найдена!</p>
                <?php endif ?>
                <form method="post" name="auth">
                    <div class="index-auth">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <?php if ($CORE->Session->isAuth()):?>
                            <tr>
                                <td>
                                    <p>Авторизация пройдена!</p>
                                </td>
                            </tr>
                            <?php else:?>
                            <tr>
                                <td class="iat">Ваш логин:
                                    <br />
                                    <input id="login_id" size="30" name="login" value="<?= $CORE->User->getLogin() ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="iat">Ваш пароль:
                                    <br />
                                    <input id="password_id" size="30" name="password" type="password"/>
                                </td>
                            </tr>
                            <?php endif;?>
                            <tr>
                                <td>
                                    <?php if ($CORE->Session->isAuth()):?>
                                        <input type="submit" name="action" value="Выйти" />
                                    <?php else:?>
                                        <input type="submit" name="action" value="Войти" />
                                        <input class="test" type="button" value="admin" style="margin-left: 5%"/>
                                        <input class="test" type="button" value="user" />
                                        <input class="test" type="button" value="author" />
                                        <input class="test" type="button" value="test" />
                                    <?php endif;?>

                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
        </td>
    </tr>
</table>

<?php require_once '.src/footer.php';
