<?
$MENU = [
    [
        'title' => 'Main',
        'path'  => '/',
        'sort'  => 100,
    ],
    [
        'title' => 'Post',
        'path'  => '/post/',
        'sort'  => 200,
    ],
    [
        'title' => 'Read posts',
        'path'  => '/post/view/',
        'sort'  => 400,
    ],
];
return $MENU;
