<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php $CORE->Site->addCss('styles.css')?>
    <?php $CORE->Site->addJs('jquery-3.3.1.js')?>
    <?php $CORE->Site->addJs('script.js')?>
    <title>Project - ведение списков</title>
</head>

<body>

<div class="header">
    <div class="logo"><a href="/"><img src="<?=$CORE->Site->getTemplate()->getRootRel()?>/res/img/i/logo.png" width="68" height="23" alt="Project" /></a></div>
    <div style="clear: both"></div>
</div>

<?php new TaskMan\Component\Menu('top_menu')?>
