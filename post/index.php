<? require_once $_SERVER['DOCUMENT_ROOT'] . '/.src/header.php' ?>
    <h1 class="router"><?= Task\Helper\Route::sectionName() ?></h1>
    <div style="color: white; font-size: large">
        <?
        $rights = $CORE->User->getRights();
        if ($rights >= 'W'): ?>
            <a href="add/" style="color: white">Написать сообщение</a>
        <? else: ?>
            Вы сможете отправлять сообщения после прохождения модерации
        <?endif; ?>
    </div>
<? require_once $_SERVER['DOCUMENT_ROOT'] . '/.src/footer.php';
