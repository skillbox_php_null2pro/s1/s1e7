<? require_once $_SERVER['DOCUMENT_ROOT'] . '/.src/header.php' ?>
<?
if ($CORE->User->getRights() < 'W'){
    \Task\Helper\Route::go('/post/');
}

$UID = $CORE->User->getId();

$isMessageWrited = false;
if (isset($_POST['message'])) {
    $isMessageWrited = (new \Task\Core\Controller\PostMessage())->parseForm($_POST['message'], $UID);
}

$Post = new \Task\Core\Controller\PostSection();

?>
    <h1 class="router"><?= Task\Helper\Route::sectionName() ?></h1>
<?if($isMessageWrited):?>
    <h3 style="color: green">Сообщение отправлено!</h3>
    <script>alert('Сообщение отправлено!')</script>
<?endif;?>
    <form style="color: white" name="message" method="post" action="index.php">
        <div style="width: 400px">
            <input name="message[title]" type="text" placeholder="Тема" required style="width: inherit"></br>
            <textarea name="message[text]" id="text" placeholder="Сообщение" required maxlength="65535" style="width: inherit; height: 200px; margin-top: 10px"></textarea></br>
            <table style="width: inherit">
                <tr>
                    <td>
                        <select name="message[receiver_id]" style="width: -webkit-fill-available;" required>
                            <option value="" disabled selected>Получатель</option>
                            <?foreach ($Post->getFromList() as $id => $name): ?>
                                <?if($id==$UID){continue;}?>
                                <option value="<?= $id ?>"><?= $name ?></option>
                            <? endforeach; ?>
                        </select>
                    </td>
                    <td>
                        <select name="message[section_id]" style="width: -webkit-fill-available;" required>
                            <option value="" disabled selected>Раздел</option>
                            <?foreach ($Post->getSectionList() as $id => $section): ?>
                                <option value="<?= $id ?>" style="background-color: <?=$section['color']?>"><?= $section['name'] ?><?=(!empty($section['child']))?':':''?></option>
                            <?if (!empty($section['child'])):?>
                                <?foreach ($section['child'] as $subId => $subSection):?>
                                        <option value="<?= $subId ?>" style="background-color: <?=$subSection['color']?>"> • <?= $subSection['name'] ?></option>
                                <?endforeach;?>
                            <?endif;?>
                            <? endforeach; ?>
                        </select>
                    </td>
                    <td align="right">
                        <input type="submit" style="width: -webkit-fill-available;">
                    </td>
                </tr>
            </table>
        </div>
    </form>
<? require_once $_SERVER['DOCUMENT_ROOT'] . '/.src/footer.php';
