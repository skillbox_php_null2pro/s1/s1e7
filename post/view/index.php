<? require_once $_SERVER['DOCUMENT_ROOT'] . '/.src/header.php' ?>
<?
$Post = new \Task\Core\Controller\PostMessage();
$detail = $Post->checkDetailPageQuery();
?>
    <style>
        .post_list, .post_list a{
            color: white;
        }
        .post_list li{
            list-style-type: decimal;
        }
        .message{
            color: white;
        }
    </style>
    <h1 class="router"><?= Task\Helper\Route::sectionName() ?></h1>
<?if($detail===false):?>
<div class="post_list">
    Непрочитанные:
    <ul>
        <?foreach ($Post->getNewPostList($CORE->User->getId(), false) as $message):?>
            <?if($message['readed']==true){continue;}?>
            <li><a href=".?id=<?=$message['id']?>" target="_blank"> <?=$message['title']?></a></li>
        <?endforeach;?>
    </ul>
    Прочитанные:
    <ul>
        <?foreach ($Post->getNewPostList($CORE->User->getId(), false) as $message):?>
            <?if($message['readed']!=true){continue;}?>
            <li><a href=".?id=<?=$message['id']?>" target="_blank"> <?=$message['title']?></a></li>
        <?endforeach;?>
    </ul>

</div>
<?else:?>
<div class="message">
    <label>Время: </label><?=$detail['date_create']?></br>
    <label>Автор: </label><?=$detail['author']['last_name'].' '.$detail['author']['first_name'].' '.$detail['author']['middle_name'].' ('.$detail['author']['email'].')'?></br>
    <label>Тема: </label><?=$detail['title']?></br>
    <label>Текст: </label><?=$detail['text']?></br>
</div>
<?endif;?>
<script>
    var reload=null;
    $(function () {
        //обновляем страницу по клику на сообщении
        $('.post_list a').click(function () {
            if (reload!==null){
                clearTimeout(reload);
                reload=null;
            }
            reload = setTimeout(function () {
                document.location.reload(true);
            }, 1000);
        });
    });
</script>
<? require_once $_SERVER['DOCUMENT_ROOT'] . '/.src/footer.php';
