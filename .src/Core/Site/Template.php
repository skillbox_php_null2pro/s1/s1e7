<?php

namespace Task\Core\Site;


use Task\Core\Config;

class Template
{
    private $name;
    private $rootRel;
    private $rootAbs;

    public function __construct($siteRoot)
    {
        $Config = Config::getInstance();

        $this->name = $Config->getKey('template','site');

        if (!$this->name) {
            $this->name = 'default';
        }

        $this->rootRel = '/include/templates/' . $this->name;

        $this->rootAbs = $siteRoot . $this->rootRel;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRootRel(): string
    {
        return $this->rootRel;
    }

    /**
     * @return string
     */
    public function getRootAbs(): string
    {
        return $this->rootAbs;
    }


}