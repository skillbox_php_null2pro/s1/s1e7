<?php

namespace Task\Core;

class Config
{
    private static $instance;
    private $config;
    private $section;
    private $path;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        $this->path = realpath(__DIR__ . '/../config.php');
        $this->config = require $this->path;
    }

    public function __destruct()
    {
        $content = '<?php' . PHP_EOL . 'return ' . var_export($this->config, true) . ';';
        if (!file_exists($this->path) || md5_file($this->path) != md5($content)) {
            file_put_contents($this->path, $content);
        }
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

    public function getSection($name)
    {
        if (isset($this->config[$name])) {
            $this->section = $name;
            return $this->config[$this->section];
        }

        return false;
    }

    public function getKey($name, $section = false)
    {
        if ($section === false) {
            $section = $this->section;
        }

        if (isset($this->config[$section][$name])) {
            return $this->config[$section][$name];
        }

        return false;
    }

    public function newSection($name): bool
    {
        if (!isset($this->config[$name])) {
            $this->section = $name;
            $this->config[$this->section] = array();
            return true;
        }

        return false;
    }

    public function newKey($name, $value, $section = false): bool
    {
        if ($section === false) {
            $section = $this->section;
        }

        if (!isset($this->config[$section][$name])) {
            $this->config[$section][$name] = $value;
            return true;
        }

        return false;
    }

    public function updateSection($name, array $value): bool
    {
        if (isset($this->config[$name])) {
            $this->section = $name;
            $this->config[$this->section] = $value;
            return true;
        }

        return false;
    }

    public function updateKey($name, $value, $section = false): bool
    {
        if ($section === false) {
            $section = $this->section;
        }

        if (isset($this->config[$section][$name])) {
            $this->config[$section][$name] = $value;
            return true;
        }

        return false;
    }

}