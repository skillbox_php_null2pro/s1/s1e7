<?php

namespace Task\Core;


class Register
{
    private static $instance;
    protected static $storage = array();

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Register();
        }

        return self::$instance;
    }

    public static function set(string $key, $value): bool
    {
        if (!isset(self::$storage[$key])) {
            self::$storage[$key] = $value;
            return true;
        } else {
            return false;
        }
    }

    public static function unset(string $key): bool
    {
        if (isset(self::$storage[$key])) {
            unset(self::$storage[$key]);
            return true;
        } else {
            return false;
        }
    }

    public static function get(string $key)
    {
        if (isset(self::$storage[$key])) {
            return self::$storage[$key];
        } else {
            return false;
        }
    }

    public static function list()
    {
        return self::$storage;
    }
}