<?php

namespace Task\Core;

class Session
{
    public function __construct()
    {
        $Config = Config::getInstance();

        $name = $Config->getKey('name', 'session');
        $time = $Config->getKey('lifetime');
        $hashType = $Config->getKey('hash');

        if (session_status() == PHP_SESSION_NONE) {
            ini_set('session.use_strict_mode', true); //принимаем только валидный id сессии
            ini_set('session.use_only_cookies', true); //храним сессии только в куках
            ini_set('session.cookie_lifetime', $time);
            if ($hashType && array_search($hashType, hash_algos()) !== false) {
                ini_set('session.hash_function', $hashType);
            }
            session_name($name);
            session_start();
        }
    }

    public function destroy()
    {
        if (session_status() == PHP_SESSION_ACTIVE) {
            setcookie(session_name(), false, true);
            session_destroy();
            unset($_SESSION);
        }
    }

    public function refresh()
    {
        if (session_status() == PHP_SESSION_ACTIVE) {
            session_regenerate_id(true);
        }
    }

    private function isActive()
    {
        return session_status() == PHP_SESSION_ACTIVE;
    }

    public function isAuth()
    {
        if (!empty($_SESSION['auth'])){
            return $_SESSION['auth'];
        }

        return false;
    }

    /**
     *Устанавливаем флаг авторизации
     */
    public function setAuth(bool $state = false)
    {
        $_SESSION['auth'] = $state;
    }

}