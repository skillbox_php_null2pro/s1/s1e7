<?php

namespace Task\Core\DB;

class User extends Table
{

    /**
     * Проверяет, существует ли в БД такая пара логин:пароль
     * @param $login
     * @param $pass
     * @return bool - результат проверки
     */
    public function authByLogin(string $login, string $pass): bool
    {
        $result=$this->getUsersByLogin($login);

        if ($result->getRowCount()){
            if ($user=$result->getNext()){
                if ($user['pass']===$pass){
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Возвращает пользователей по логину
     * @param string $login
     * @return Result
     */
    public function getUsersByLogin(string $login): Result
    {
        $sql = 'select * FROM c_auth_user where login=:login';
        $psql = $this->pdo->prepare($sql);
        $psql->bindParam(':login', $login);
        $psql->execute();

        return new Result($psql);
    }

    /**
     * Возвращает пользователей по UID
     * @param array $arUIDs
     * @return Result
     */
    public function getUsersByUIDs(array $arUIDs): Result
    {
        $sql = 'select * FROM c_auth_user where id in (' . implode(',', array_fill(0, count($arUIDs), '?')) . ')';
        $psql = $this->pdo->prepare($sql);
        $psql->execute($arUIDs);

        return new Result($psql);
    }

    /**
     * Возвращает список пользователей
     * @return Result
     */
    public function getList(): Result
    {
        $sql = 'select * FROM c_auth_user';
        $psql = $this->pdo->prepare($sql);
        $psql->execute();

        return new Result($psql);
    }
}