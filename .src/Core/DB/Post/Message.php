<?php

namespace Task\Core\DB\Post;

use Task\Core\DB\Result;
use Task\Core\DB\Table;

class Message extends Table
{
    private $id;
    private $section_id;
    private $author_id;
    private $receiver_id;
    private $date_create;
    private $title;
    private $text;
    private $readed;

    /**
     * Добавляет новое сообщение
     * @param $message = [
     * 'section_id' => 123,
     * 'author_id' => 123,
     * 'receiver_id' => 123,
     * 'title' => 'text',
     * 'text' => 'text',
     * ]
     * @return bool|int - возвращает false или id добавленной записи
     */
    public function _addMessageAsArray(array $message)
    {
        try {
            $this->setSectionId($message['section_id']);
            $this->setAuthorId($message['author_id']);
            $this->setReceiverId($message['receiver_id']);
            $this->setTitle($message['title']);
            $this->setText($message['text']);
        } catch (\InvalidArgumentException $e) {
            return false;
        }

        return $this->_addNewMessageToDB();
    }

    public function _addNewMessageToDB()
    {
        $res = $this->__writeMessage($this->section_id, $this->author_id, $this->receiver_id, $this->title, $this->text);
        $error = !$res->getRowCount();

        if (!$error) {
            $this->id = $this->DB->getLastInsertedID();
            return $this->id;
        }

        return false;
    }

    //полный формат вызова - для рантайм проверки формата и выброса исключений при несоответствии
    private function __writeMessage(int $section_id, int $author_id, int $receiver_id, string $title, string $text): Result
    {
        $sql = 'insert into c_post_message set section_id=?, author_id=?, receiver_id=?, title=?, text=?';
        $psql = $this->pdo->prepare($sql);
        $psql->execute([
            $section_id,
            $author_id,
            $receiver_id,
            $title,
            $text,
        ]);

        return new Result($psql);
    }

    public function _getListMessageTitleFromDB(int $userID): Result
    {
        $sql = 'select id,title,readed from c_post_message where receiver_id=?';
        $psql = $this->pdo->prepare($sql);
        $psql->execute([$userID]);

        return new Result($psql);
    }

    public function _getMessageFromDB(int $messageID){
        $sql = 'select * from c_post_message where id=?';
        $psql = $this->pdo->prepare($sql);
        $psql->execute([$messageID]);

        return new Result($psql);
    }

    public function _setReadingFlagForMessageFromDB(int $messageID){
        $sql = 'update c_post_message set readed=1 where id=?';
        $psql = $this->pdo->prepare($sql);
        $psql->execute([$messageID]);

        return new Result($psql);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSectionId()
    {
        return $this->section_id;
    }

    /**
     * @return mixed
     */
    public function getAuthorId()
    {
        return $this->author_id;
    }

    /**
     * @return mixed
     */
    public function getReceiverId()
    {
        return $this->receiver_id;
    }

    /**
     * @return mixed
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getReaded()
    {
        return $this->readed;
    }

    /**
     * @param mixed $section_id
     */
    public function setSectionId($section_id)
    {
        $this->section_id = $section_id;
    }

    /**
     * @param mixed $author_id
     */
    public function setAuthorId($author_id)
    {
        $this->author_id = $author_id;
    }

    /**
     * @param mixed $receiver_id
     */
    public function setReceiverId($receiver_id)
    {
        $this->receiver_id = $receiver_id;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }


}