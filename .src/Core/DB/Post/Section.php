<?php

namespace Task\Core\DB\Post;

use Task\Core\DB\Result;
use Task\Core\DB\Table;

class Section extends Table
{
    private $id;
    private $parent_id;
    private $name;
    private $color;
    private $date_crate;
    private $owner_id;

    public function getList(): Result
    {
        $sql = 'select * FROM c_post_section';
        $psql = $this->pdo->prepare($sql);
        $psql->execute();

        return new Result($psql);
    }
}