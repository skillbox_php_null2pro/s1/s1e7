<?php

namespace Task\Core\DB;

class UserGroup extends Table
{
    public function getGIDByUID($uid): Result
    {
        $sql = 'select group_id FROM c_auth_user_group where user_id=:uid';
        $psql = $this->pdo->prepare($sql);
        $psql->bindParam(':uid', $uid);
        $psql->execute();

        return new Result($psql);
    }

    public function getUIDByGID($gid): Result
    {
        $sql = 'select user_id FROM c_auth_user_group where group_id=:gid';
        $psql = $this->pdo->prepare($sql);
        $psql->bindParam(':gid', $gid);
        $psql->execute();

        return new Result($psql);
    }
}