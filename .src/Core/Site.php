<?php

namespace Task\Core;


use Task\Core;
use Task\Core\Site\Template;

class Site
{
    private $root;

    /**@var Template */
    private $Template;

    public function __construct()
    {
        $Config = Config::getInstance();

        $this->root = $Config->getKey('root', 'site');
        $this->Template = new Template($this->root);
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->Template;
    }

    public function addCss($fileName)
    {
        /**@var $CORE Core */
        $CORE = Register::get('core');
        echo '<link href="';
        echo $CORE->Site->getTemplate()->getRootRel();
        echo '/res/css/';
        echo $fileName;
        echo '" rel="stylesheet" />';
    }

    public function addJs($fileName)
    {
        /**@var $CORE Core */
        $CORE = Register::get('core');
        echo '<script src="';
        echo $CORE->Site->getTemplate()->getRootRel();
        echo '/res/js/';
        echo $fileName;
        echo '"></script>';
    }


}