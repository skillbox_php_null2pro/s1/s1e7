<?php

namespace Task\Core;


use function PHPSTORM_META\type;
use Task\Core\DB\Group;
use Task\Core\DB\Result;
use Task\Core\DB\UserGroup;
use Task\Helper\General;

class User
{
    private $id;
    private $active;
    private $last_name;
    private $first_name;
    private $middle_name;
    private $email;
    private $phone;
    private $login;
    private $pass;
    private $spam;
    private $groups;
    private $rights;

    public function __construct(string $login = null)
    {
        $this->login = $login;
    }

    public function authByLogin(string $login, string $pass): bool
    {
        $this->login = $login;
        $this->pass = $pass;

        $dbUser = new \Task\Core\DB\User();
        $auth = $dbUser->authByLogin($this->login, $this->pass);

        return $auth;
    }

    public function fillUserInfo()
    {
        if (empty($_SESSION['login'])) {
            return false;
        }

        $dbUser = new \Task\Core\DB\User();
        $res = $dbUser->getUsersByLogin($this->login);
        if ($res->getRowCount()) {
            $User = $res->getNext();
            foreach ($User as $key => $value) {
                $this->{$key} = $value;
            }
        }

        $dbUserGroup = new UserGroup();
        $res = $dbUserGroup->getGIDByUID($this->id);
        $arGIDs = [];
        if ($res->getRowCount()) {
            $arGIDs = array_column($res->getAll(), 'group_id');
        }
        $this->groups = $arGIDs;

        $dbGroup = new Group();
        $res = $dbGroup->getPostRightsByGIDs($arGIDs);
        $rights = 'R';
        if ($res->getRowCount()) {
            $rights = array_column($res->getAll(), 'post');
            sort($rights);
            $rights = array_pop($rights);
        }
        $this->rights = $rights;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @return mixed
     */
    public function getSpam()
    {
        return $this->spam;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return mixed
     */
    public function getRights()
    {
        return $this->rights;
    }



}