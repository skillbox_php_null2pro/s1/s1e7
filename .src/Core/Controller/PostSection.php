<?php

namespace Task\Core\Controller;

use Task\Core\Config;
use Task\Core\DB\Post\Section;

class PostSection
{
    private $fromList;
    private $sectionList;

    private function getUserIdsFromList(int $gid)
    {
        $dbUserGroup = new \Task\Core\DB\UserGroup();
        $res = $dbUserGroup->getUIDByGID($gid);
        $arUserIds = [];
        if ($res->getRowCount()) {
            $arUserIds = array_column($res->getAll(), 'user_id');
        }

        return $arUserIds;
    }

    private function getUsersFromList($arUserIds)
    {
        $dbUser = new \Task\Core\DB\User();
        $res = $dbUser->getUsersByUIDs($arUserIds);
        $arUsers = [];
        if ($res->getRowCount()) {
            $arUsers = $res->getAll();
        }

        if (count($arUsers)) {
            $arUsers = array_column($arUsers, 'first_name', 'id');
        }

        return $arUsers;
    }

    private function getDbSectionList()
    {
        $dbSection = new Section();
        $res = $dbSection->getList();
        $arSections = [];
        if ($res->getRowCount()) {
            $arSections = $res->getAll();
        }

        $arSections = array_column($arSections, null, 'id');

        //форматируем дочерние секции
        foreach ($arSections as $id => $section) {
            if ($section['parent_id'] === null) {continue;}
            unset($arSections[$id]);
            $arSections[$section['parent_id']]['child'][$section['id']] = $section;
        }

        return $arSections;
    }

    /**
     * @return array
     */
    public function getFromList(): array
    {
        if (empty($this->fromList)) {
            $Config = Config::getInstance();
            $gid = $Config->getKey('author', 'gid');
            $arUserIds = $this->getUserIdsFromList($gid);
            $this->fromList = $this->getUsersFromList($arUserIds);
        }

        return $this->fromList;
    }

    /**
     * @return array
     */
    public function getSectionList(): array
    {
        if (empty($this->sectionList)) {
            $this->sectionList = $this->getDbSectionList();
        }

        return $this->sectionList;
    }


}