<?php

namespace TaskMan\Component;

use Task\Core;
use Task\Core\Component;
use Task\Core\Register;
use Task\Helper\General;
use Task\Helper\Route;

class Menu extends Component
{
    /**Модуль, выводящий меню
     *
     * @param string $template - шаблон меню
     * @param string $sort - порядок сортировки пунктов
     */
    public function __construct($template, $sort = 'asc')
    {
        $this->template = $template;
        $this->settings['sort'] = $sort;
        $this->result['menu'] = General::getMenu();
        $this->exec();
    }

    public function exec()
    {
        /**@var $CORE Core */
        $CORE = Register::get('core');

        if (!$CORE->Session->isAuth()) {
            return;
        }

        if (!$this->template) {
            return;
        }

        $this->result['menu'] = General::arraySort($this->result['menu'], 'sort', $this->settings['sort']);

        $path = Route::getSectionPath();
        foreach ($this->result['menu'] as $id => $section) {
            $this->result['menu'][$id]['selected'] = ($section['path'] == $path);
        }

        $this->includeTemplate();
    }
}