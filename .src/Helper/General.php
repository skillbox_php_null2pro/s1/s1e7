<?php

namespace Task\Helper;

use Task\Core;
use Task\Core\Register;

class General
{
    /**
     * Возвращает глобальное меню
     * @return array $MENU
     */
    public static function getMenu()
    {
        /**@var $CORE Core */
        $CORE = Register::get('core');
        return require $CORE->Config->getKey('root', 'site') . '/main_menu.php';
    }

    /**
     * Обрезает заголовок
     * @param      $title
     * @param bool $return - вернуть новый заголовок, или изменить по ссылке
     *
     * @return string
     */
    public static function titleCrop($title)
    {
        if (strlen($title) > 15) {
            $title = substr($title, 0, 12) . '...';
        }

        return $title;
    }

    /**
     * Сортирует массив по ключу sort
     * @param $array
     * @param string $key
     * @param string $order
     */
    public static function arraySort($array, $key = 'sort', $order = 'asc')
    {
        uasort(
            $array,
            function ($a, $b) use ($key, $order) {
                return ($order == 'desc') ? $b[$key] <=> $a[$key] : $a[$key] <=> $b[$key];
            }
        );

        return $array;
    }

    /**
     * Отладочная функция
     * @param $mixed
     */
    public static function debug($mixed, $die=true)
    {
        echo '<pre>';
        if ($mixed instanceof Core\DB\Result){
            echo 'RowsCount: '.$mixed->getRowCount().PHP_EOL;
            var_export($mixed->getAll());
        } else {
            print_r($mixed);
        }
        if ($die) {
            die();
        }
    }
}
