<?php

namespace Task\Helper;

class Route
{
    /**
     * Переадресовывает в
     * @param string $url
     */
    public static function go(string $url = '/')
    {
        header('Location: ' . $url);
        exit();
    }

    /**
     * Возвращает true, если путь не существует
     * @return bool
     */
    public static function is404()
    {
        $result = false;
        $result = $result || preg_match("/^\/$|^\/[?][^\/]*/", $_SERVER['REQUEST_URI']);// root
        $result = $result || preg_match("/^\/index.php$|^\/index.php[?][^\/]*/", $_SERVER['REQUEST_URI']);// /index.php
        $result = $result || self::getSectionPath() !== false;
        $result = $result || preg_match("/^\/post\/.*/", $_SERVER['REQUEST_URI']);// post
        return !$result;
    }

    /**
     * Возвращает false или путь к секции
     * @return false|string путь к секции в корректном формате
     */
    public static function getSectionPath()
    {
        preg_match("/\/route\/\w*(\/?)/", $_SERVER['REQUEST_URI'], $matches); // /route

        if (!$matches) {
            return $_SERVER['REQUEST_URI'];
        }

        if (!$matches[1]) {
            $matches[0] .= '/';
        }

        return $matches[0];
    }

    /**
     * Получает название раздела
     * @return null|string
     */
    public static function sectionName()
    {
        $menu = General::getMenu();
        $path = self::getSectionPath();

        foreach ($menu as $section) {
            if ($section['path'] == $path) {
                return $section['title'];
            }
        }

        return ucfirst(str_replace(DIRECTORY_SEPARATOR, '', dirname($path)));
    }

}