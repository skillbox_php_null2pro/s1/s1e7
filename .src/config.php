<?php
return array (
  'gid' => 
  array (
    'author' => 3,
  ),
  'db' => 
  array (
    'host' => 'localhost',
    'dbname' => 'task',
    'user' => 'root',
    'pass' => '',
  ),
  'site' => 
  array (
    'root' => '/home/tomas/PhpstormProjects/skillbox',
    'template' => 'default',
  ),
  'session' => 
  array (
    'name' => 'session_id',
    'lifetime' => 1200,
    'hash' => 'sha256',
  ),
  'cookie' => 
  array (
    'login' => 
    array (
      'lifetime' => 4320000,
    ),
  ),
);